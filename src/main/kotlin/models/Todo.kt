package models

import java.util.UUID
import kotlinx.datetime.LocalDate
import kotlinx.serialization.Serializable
import java.lang.IllegalArgumentException


const val Black = "\u001b[30m"
const val Red = "\u001b[31m"
const val Green = "\u001b[32m"
const val Yellow = "\u001b[33m"
const val Blue = "\u001b[34m"
const val Magenta = "\u001b[35m"
const val Cyan = "\u001b[36m"
const val White = "\u001b[37m"
const val Reset = "\u001b[0m"


enum class PRIORITY(val value: Int) {
    LOWEST(5),
    LOW(4),
    MEDIUM(3),
    HIGH(2),
    HIGHEST(1)
}

enum class STATUS {
    IN_PROGRESS,
    BACKLOG,
    COMPLETE
}

@Serializable
data class Todo(
    val name: String,
    val priority: PRIORITY,
    val description: String? = null,
    val children: List<Todo> = listOf(),
    var id: Int = 0
) {

    private fun description(): String = description?.run { "\n" + "| ".padStart(6) + description } ?: ""

    override fun toString(): String =
        when (priority) {
            PRIORITY.HIGHEST -> Red
            PRIORITY.HIGH -> Yellow
            PRIORITY.MEDIUM -> Green
            PRIORITY.LOW -> Cyan
            PRIORITY.LOWEST -> White
        } +
        "${id.toString().padStart(4)}: $name | $priority${description()}"

}

@Serializable
data class TodoList(
    val todoList: List<Todo> = listOf(),
    val finishedTodoList: List<Todo> = listOf(),
    val backlogTodoList: List<Todo> = listOf()
) {
    private val List<Todo>.highestIndex get() = if(isEmpty()) 0 else maxOf { it.id }

    fun addTodo(todo: Todo): TodoList {
        todo.id = todoList.highestIndex + 1
        return copy(todoList = todoList.plus(todo))
    }

    fun addBacklog(todo: Todo): TodoList {
        todo.id = backlogTodoList.highestIndex + 1
        return (copy(backlogTodoList = backlogTodoList.plus(todo)))
    }

    fun addFinishedTodo(todo: Todo): TodoList {
        return (copy(finishedTodoList = finishedTodoList.plus(todo)))
    }

    fun getTodo(id: Int): Todo {
        return todoList.find { it.id == id } ?: throw IllegalArgumentException()
    }

    fun getBacklog(id: Int): Todo {
        return backlogTodoList.find { it.id == id } ?: throw IllegalArgumentException()
    }

    fun getFinishedTodo(id: Int): Todo {
        return finishedTodoList.find { it.id == id } ?: throw IllegalArgumentException()
    }

    fun removeTodo(todo: Todo): TodoList =
        copy(
            todoList = todoList
                .minus(todo)
                .map {
                    if (it.id >= todo.id) it.id -= 1
                    it
                }
        )

    fun removeBacklog(todo: Todo): TodoList =
        copy(
            backlogTodoList = backlogTodoList
                .minus(todo)
                .map {
                    if (it.id >= todo.id) it.id -= 1
                    it
                }
        )
}
