package models

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

class TodoJSON(private val todoFile: File) : TodoOK {

    private var todoList: TodoList = try {
        Json.decodeFromString(todoFile.readText())
    } catch (e: java.lang.Exception) {
        TodoList()
    }


    override fun commit() {
        todoFile.writeText(
            Json.encodeToString(
                todoList
            )
        )
    }

    override fun addTodo(todo: Todo, list: ListType): Todo {
        todoList = when (list) {
            ListType.MAIN -> todoList.addTodo(todo)
            ListType.BACKLOG -> todoList.addBacklog(todo)
            ListType.FINISHED -> todoList.addFinishedTodo(todo)
        }
        return todo
    }

    @Throws
    override fun removeTodo(id: Int, list: ListType) =
        when (list) {
            ListType.MAIN -> {
                val todo = todoList.getTodo(id)
                todoList = todoList.removeTodo(todo)
                todo
            }
            ListType.BACKLOG -> {
                val todo = todoList.getBacklog(id)
                todoList = todoList.removeBacklog(todo)
                todo
            }
            ListType.FINISHED -> {
                throw IllegalArgumentException()
            }
        }

    override fun getTodo(id: Int, list: ListType): Todo =
        todoList.todoList.find { it.id == id } ?: throw java.lang.IllegalArgumentException()

    override fun getTodos(priority: PRIORITY?, list: ListType): List<Todo> =
        todoList.todoList.filter {
            priority == null
            || it.priority == priority
        }.sortedBy { it.priority }

    override fun clear() {
        todoFile.writeText("")
    }

}