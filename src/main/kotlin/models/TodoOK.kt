package models

interface TodoOK {
    fun addTodo(todo: Todo, list: ListType): Todo
    fun removeTodo(id: Int, list: ListType): Todo
    fun getTodo(id: Int, list: ListType): Todo
    fun getTodos(priority: PRIORITY? = null, list: ListType): List<Todo>
    fun commit()
    fun clear()
}
