package models

enum class ListType {
    MAIN,
    BACKLOG,
    FINISHED,
}