import kotlinx.cli.*
import models.*

fun priority(value: String?): PRIORITY? {
    if (value == null) return null
    if (value == "") return PRIORITY.MEDIUM
    return try {
        PRIORITY.valueOf(value.uppercase())
    } catch (e: IllegalArgumentException) {
        null
    }
}


@OptIn(ExperimentalCli::class)
class Add(private val todoOK: TodoOK) : Subcommand("add", "Adds a todo") {

    val complex by option(
        ArgType.Boolean,
        shortName = "c",
        fullName = "complex",
        description = "Adds a complex todo. Due Date and Description"
    ).default(false)

    val backlog by option(
        ArgType.Boolean,
        shortName = "b",
        fullName = "backlog",
        description = "Adds a backlog todo"
    ).default(false)

    private fun standardTodo(): Todo {
        println("Whats the todo?")
        val name = readln()
        var priority: PRIORITY? = null
        println("What's the priority")
        while (priority == null) {
            priority = priority(readln())
            if (priority == null) println("Invalid Priority, try again")
        }
        return Todo(name = name, priority = priority)
    }

    private fun complexTodo(): Todo {
        val baseTodoOK = standardTodo()
        println("Add a description. Accepts new lines.")
        val lines = mutableListOf<String?>()
        var currentLine: String? = null
        while (currentLine != "") {
            currentLine = readLine()
            lines.add(currentLine)
        }
        val description = lines.joinToString(" ")
        return baseTodoOK.copy(
            description = description
        )
    }

    override fun execute() {
        val todo: Todo = if (complex) complexTodo() else standardTodo()
        todoOK.addTodo(todo, ListType.MAIN)
        todoOK.commit()
    }
}

@OptIn(ExperimentalCli::class)
class Remove(private val todoOK: TodoOK) : Subcommand("remove", "Removes a specified todo") {
    private val id by argument(ArgType.Int, "id", description = "Id of the todo you've completed")
    private val backlog by option(
        ArgType.Boolean,
        "backlog",
        "b",
        description = "Id of the todo you've completed"
    ).default(false)

    override fun execute() {
        val list = if (backlog) ListType.BACKLOG else ListType.MAIN
        todoOK.removeTodo(id, list)
        todoOK.commit()
    }
}

@OptIn(ExperimentalCli::class)
class ListTodo(private val todoOK: TodoOK) : Subcommand("list", "Lists todos") {
    private val priority by argument(ArgType.String, "priority", description = "Priority").optional()
    private val backlog by option(ArgType.Boolean, "backlog", "b", "Lists out backlog todos").default(false)
    private val finished by option(ArgType.Boolean, "finished", "f", "Lists out finished todos").default(false)

    fun List<Todo>.print() =
        forEach {
            println(it)
        }

    override fun execute() {
        when {
            backlog && finished -> {
                println("BACKLOG")
                println("---")
                todoOK.getTodos(list = ListType.BACKLOG).print()
                println("FINISHED")
                println("---")
                todoOK.getTodos(list = ListType.FINISHED).print()
            }
            backlog ->
                todoOK.getTodos(list = ListType.BACKLOG).print()
            finished ->
                todoOK.getTodos(list = ListType.FINISHED).print()
            else -> todoOK.getTodos(
                priority(priority),
                ListType.MAIN
            ).print()
        }
    }
}

@OptIn(ExperimentalCli::class)
class Finish(private val todoOK: TodoOK) : Subcommand("finish", "Finishes todo") {
    private val id by argument(ArgType.Int, "id", description = "Id to finish")
    private val fromBacklog by option(ArgType.Boolean, "from_backlog", "b", "Finish todo from backlog").default(false)
    override fun execute() {
        val list = if (fromBacklog) ListType.BACKLOG else ListType.MAIN
        try {
            val todo = todoOK.removeTodo(id, list)
            todoOK.addTodo(todo, ListType.FINISHED)
        } catch (e: IllegalArgumentException) {
            println("${Red}Invalid id")
        }
    }
}

@OptIn(ExperimentalCli::class)
class Backlog(private val todoOK: TodoOK) : Subcommand("backlog", "Backlog a todo") {
    private val id by argument(ArgType.Int, "id", description = "Id to backlog")
    private val invert by option(ArgType.Boolean, "invert", "v", "Moves backlog todo to main todo list").default(false)
    override fun execute() {
        val list = if (invert) ListType.BACKLOG else ListType.MAIN
        try {
            todoOK.removeTodo(id, list)
            todoOK.commit()
        } catch (e: IllegalArgumentException) {
            println("${Red}Invalid id")
        }
    }
}

@OptIn(ExperimentalCli::class)
class Clear(private val todoOK: TodoOK) : Subcommand("clear", "Clears all todos") {
    val yeses = listOf("yes", "y", "yeah", "fuck yeah")
    override fun execute() {
        println(Magenta + "Are you Sure? [Y/N]")
        val yes = readln()
        if (yeses.contains(yes.lowercase())) todoOK.clear()
    }
}
