import kotlinx.cli.*
import models.*
import java.io.File

@OptIn(ExperimentalCli::class)
fun main(args: Array<String>) {
    val path = System.getProperty("user.dir")
    val todoOK = TodoJSON(File("$path/.todook.json"))
    val parser = ArgParser("example")

    val add = Add(todoOK)
    val remove = Remove(todoOK)
    val backlog = Backlog(todoOK)
    val finish = Finish(todoOK)
    val list = ListTodo(todoOK)
    val clear = Clear(todoOK)

    parser.subcommands(add, remove, backlog, finish, list, clear)
    parser.parse(args)
}